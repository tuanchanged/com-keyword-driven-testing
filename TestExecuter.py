import xlrd
from TestLibrary import TestLibrary
from _operator import sub

DATA_FILE = "DataFile.xlsx"

SHEET_NAME_TESTCASES = "Test Cases"
SHEET_NAME_USER_KEYWORD = "User Keyword"

USER_KEYWORD_STR = "User"
BASE_KEYWORD_STR = "Base"
ARGS_KEYWORD_STR = "Args"


class Keyword:

    def __init__(self):
        self.__name = ""
        self.__type = BASE_KEYWORD_STR
        self.__args = [] 
    
    def getName(self):
        return self.__name

    def setName(self, name):
        self.__name = name
        
    def getType(self):
        return self.__type

    def setType(self, type):
        self.__type = type

    def getArgs(self):
        return self.__args
    
    def setArgs(self, args):
        self.__args = args

    
class TestCase:

    def __init__(self):
        self.__name = ""
        self.__doc = ""
        self.__keywords = []
    
    def getName(self):
        return self.__name
    
    def setName(self, name):
        self.__name = name
    
    def getDoc(self):
        return self.__doc
    
    def setDoc(self, doc):
        self.__doc = doc
        
    def getKeywords(self):
        return self.__keywords
    
    def appendKeyword(self, keyword):
        self.__keywords.append(keyword)
        
    def setKeywords(self, keywords):
        self.__keywords = keywords


class ExcelUtils:

    def __init__(self, excel_file, sheet_name):
        self.excel_file = excel_file
        self.sheet_name = sheet_name
    
    def parser(self):
        try:
            workbook = xlrd.open_workbook(self.excel_file)
            sheet = workbook.sheet_by_name(self.sheet_name)
    
            table = []
            for nrow in range(0, sheet.nrows):
                table.append(sheet.row_values(nrow))
    
            workbook.release_resources()
            return table
        except Exception as err:
            raise(err)


class KeywordDrivenParser:

    def __init__(self):
        self.lines = ExcelUtils(DATA_FILE, SHEET_NAME_TESTCASES).parser()
        self.uklines = ExcelUtils(DATA_FILE, SHEET_NAME_USER_KEYWORD).parser()
    
    def getTestCases(self):
        testCasesData = []  
        testCaseData = TestCase()
        
        for line in self.lines[2:]:
                     
            if (line[0] != '#####') and ("" != line[0]):
                testCaseData.setName(line[0])
                testCaseData.setDoc(line[1])

            if (line[0] != '#####'):
                keyword = Keyword()
                keyword.setType(line[2])
                keyword.setName(line[3])
                keyword.setArgs(line[4:])
                testCaseData.appendKeyword(keyword)
            
            if (line[0] == '#####'):
                testCasesData.append(testCaseData)
                testCaseData = TestCase()

        return testCasesData
    
    def getUserKeywords(self):
        testCasesData = []  
        testCaseData = TestCase()
               
        for line in self.uklines[2:]:
                      
            if (line[0] != '#####') and ("" != line[0]):
                testCaseData.setName(line[0])
                testCaseData.setDoc(line[1])

            if (line[0] != '#####'):
                keyword = Keyword()
                keyword.setType(line[2])
                keyword.setName(line[3])
                keyword.setArgs(line[4:])
                testCaseData.appendKeyword(keyword)
            
            if (line[0] == '#####'):
                testCasesData.append(testCaseData)
                testCaseData = TestCase()  
                
        return testCasesData


testData = KeywordDrivenParser()
testCasesData = testData.getTestCases()
userKeywordsData = testData.getUserKeywords()
print("Total of test cases : ", len(testCasesData))


def updateListString(dict_keyword_updated, old_list):
    # print("updateListString ({}, {})".format(dict_keyword_updated, "----", old_list))
    new_list = []
    for key in dict_keyword_updated.keys():
        for string in old_list:
            if key in string:
                new_list.append(string.replace(key, dict_keyword_updated[key]))
            else:
                new_list.append(string)
    return new_list


arg_dict = {}
arg_keys = []
arg_values = []

    
def executeKeyword(keyword):
    global arg_dict
    global arg_keys
    global arg_values
    kname = keyword.getName()
    ktype = keyword.getType()
    kargs = list(filter(None, keyword.getArgs()))
    
    if(BASE_KEYWORD_STR == ktype):
        handleName = kname.lower().replace(' ', '') + 'Handler'
        
        if not hasattr(TestLibrary, handleName):
            raise Exception('No handle for keyword ', handleName)
             
        handler = getattr(TestLibrary, handleName)
        handler(TestLibrary, *kargs)
    
    elif(USER_KEYWORD_STR == ktype or ARGS_KEYWORD_STR == ktype):
        
        if(USER_KEYWORD_STR == ktype):
            print("|- Executing user keyword {:14} ({})".format(kname, kargs))
            arg_values = kargs
            userKeywordData = Keyword()
            
            for userKeywords in userKeywordsData:
                # If user keyword exists
                if(kname == userKeywords.getName()):
                    userKeywordData = userKeywords
            
            for subKeyword in userKeywordData.getKeywords():
                if(ARGS_KEYWORD_STR != subKeyword.getType()):
                    # Update arguments
                    current_args = list(filter(None, subKeyword.getArgs()))
                    updated_args = updateListString(arg_dict, current_args)
                    subKeyword.setArgs(updated_args)
                  
                executeKeyword(subKeyword)  
        else:
            arg_keys = list(filter(None, keyword.getArgs()))
            arg_dict = dict(zip(arg_keys, arg_values))
    else:
       pass


for tc in testCasesData:
    print("\nExecuting test case {} ({})".format(tc.getName(), tc.getDoc()))
    try:
        for keyword in tc.getKeywords():
            executeKeyword(keyword)
    except AssertionError as err:
        raise("Verification failed : {}".format(err))
    except Exception as err:
        raise("Error in test execution: {}".format(err))
