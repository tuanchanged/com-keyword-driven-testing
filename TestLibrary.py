class TestLibrary:

	def __init__(self, name):
		pass
	
	def openBrowser(self, url):
		pass
	
	def setText(self, obj, text):
		pass
	
	def sendKey(self, key):
		pass
	
	def hasText(self, text):
		pass
	
	def hasTextInRange(self, range):
		pass
	
	def launchHandler(self, url) :
		print("\t|- call launchHandler( {} ) ".format(url))
		
	def checktitleHandler(self , expected) :
		print("\t|- call checktitleHandler( {} ) ".format(expected))
	
	def inputHandler(self, fieldId, text):
		print("\t|- call inputHandler( {}, {} ) ".format(fieldId, text))
	
	def clickbuttonHandler(self, buttonId) :
		print("\t|- call clickbuttonHandler( {} ) ".format(buttonId))
	
	def submitHandler(self, buttonId) :
		print("\t|- call submitHandler( {} ) ".format(buttonId))
	
	def clicklinkHandler(self, linkId) :
		print("\t|- call clicklinkHandler( {} ) ".format(linkId))
	
	def clicklinkwithtextHandler(self, text) :
		print("\t|- call clicklinkwithtextHandler( {} ) ".format(text))
	
	def clicklinkwithimageHandler(self, src) :	
		print("\t|- call clicklinkwithimageHandler( {} ) ".format(src))